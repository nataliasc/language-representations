import random
import time
import numpy as np
import torch
import torch.optim as optim
import transformers
import argparse
from torch.utils.tensorboard import SummaryWriter
import time
from distutils.util import strtobool
from sac import LlamaSAC
from rb import Buffer
import gymnasium as gym
import gridworld
import os


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--exp-name",
        type=str,
        default="LLaMA gridworld",
        help="the name of this experiment",
    )
    parser.add_argument("--seed", type=int, default=1, help="seed of the experiment")
    parser.add_argument(
        "--model",
        type=str,
        default="meta-llama/Llama-2-7b-chat-hf",
        help="Llama model to extract embeddings from",
    )
    parser.add_argument(
        "--torch-deterministic",
        type=lambda x: bool(strtobool(x)),
        default=True,
        nargs="?",
        const=True,
        help="if toggled, `torch.backends.cudnn.deterministic=False`",
    )
    parser.add_argument(
        "--cuda",
        type=lambda x: bool(strtobool(x)),
        default=True,
        nargs="?",
        const=True,
        help="if toggled, cuda will be enabled by default",
    )
    parser.add_argument(
        "--track",
        type=lambda x: bool(strtobool(x)),
        default=False,
        nargs="?",
        const=True,
        help="Track with Weights and Biases",
    )
    parser.add_argument(
        "--wandb-project-name",
        type=str,
        default="Griworld",
        help="WandB project name",
    )
    parser.add_argument(
        "--wandb-entity",
        type=str,
        default="",
        help="WandB entity for this project",
    )

    parser.add_argument(
        "--hidden",
        type=int,
        default=512,
        help="Hidden dims of the policy MLP",
    )

    # Layer
    parser.add_argument(
        "--emb-layer",
        type=int,
        default=-2,
        help="Layer whose embeddings to use",
    )

    parser.add_argument(
        "--env-id", type=str, default="Gridworld", help="ID of the environment"
    )
    parser.add_argument(
        "--num-steps", type=int, default=15, help="Max episode length in the gridworld"
    )

    # SAC parameters
    parser.add_argument(
        "--num-episodes",
        type=int,
        default=1_000,
        help="N of episodes to complete",
    )
    parser.add_argument(
        "--buffer-size",
        type=int,
        default=int(100_000),
        help="Replay buffer size",
    )
    parser.add_argument("--gamma", type=float, default=0.99, help="Discount factor")

    parser.add_argument(
        "--rho", type=float, default=0.01, help="Target smoothing coefficient"
    )
    parser.add_argument(
        "--batch-size",
        type=int,
        default=24,
        help="Batch size of replay buffer sample",
    )
    parser.add_argument(
        "--learning-starts", type=int, default=250, help="Timestep to start learning"
    )
    parser.add_argument(
        "--policy-lr",
        type=float,
        default=0.001,
        help="Learning rate of the policy network optimizer",
    )
    parser.add_argument(
        "--q-lr",
        type=float,
        default=0.001,
        help="Learning rate of the Q-network optimizer",
    )
    parser.add_argument(
        "--alpha", type=float, default=0.1, help="Entropy regularization coefficient"
    )
    args = parser.parse_args()
    return args


def get_embeddings(text_inputs, emb_layer, model, tokenizer, device):
    inputs = tokenizer(text_inputs, return_tensors="pt")
    text_features = (
        model(
            inputs.input_ids.to(device),
            return_dict=True,
            output_hidden_states=True,
        )
        .hidden_states[-(emb_layer)][:, -1, :]
        .detach()
        .cpu()
    )
    text_features = text_features.to(torch.float32)
    return text_features


def mean_embeddings(text_inputs, model, tokenizer):
    inputs = tokenizer(text_inputs, return_tensors="pt")
    text_features = (
        model(
            inputs.input_ids.to(device),
            return_dict=True,
            output_hidden_states=True,
        )
        .hidden_states[-1]
        .detach()
        .cpu()
    )
    weights_for_non_padding = inputs.attention_mask * torch.arange(
        start=1, end=text_features.shape[1] + 1
    ).unsqueeze(0)
    sum_embeddings = torch.sum(
        text_features * weights_for_non_padding.unsqueeze(-1), dim=1
    )
    num_of_none_padding_tokens = torch.sum(weights_for_non_padding, dim=-1).unsqueeze(
        -1
    )
    sentence_embeddings = sum_embeddings / num_of_none_padding_tokens
    sentence_embeddings = sentence_embeddings.to(torch.float32)
    return sentence_embeddings


if __name__ == "__main__":
    os.environ['PYTORCH_ENABLE_MPS_FALLBACK'] = '1'
    args = parse_args()

    run_name = f"gridworld_llama_{time.strftime('%Y-%m-%d_%H-%M-%S')}"
    if args.track:
        import wandb

        wandb.init(
            project=args.wandb_project_name,
            entity=args.wandb_entity,
            sync_tensorboard=True,
            config=vars(args),
            name=run_name,
            monitor_gym=False,
            save_code=True,
        )
    writer = SummaryWriter(f"runs/{run_name}")
    writer.add_text(
        "hyperparameters",
        "|param|value|\n|-|-|\n%s"
        % ("\n".join([f"|{key}|{value}|" for key, value in vars(args).items()])),
    )

    # seeding
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.backends.cudnn.deterministic = args.torch_deterministic

    env = gym.make("Gridworld-v0", goal=(2, 3), starting_position=(1, 1))
    config = transformers.AutoConfig.from_pretrained(args.model)
    model = transformers.AutoModelForCausalLM.from_pretrained(
        args.model,
        config=config,
        torch_dtype=torch.bfloat16,
        device_map="auto",
    )
    
    tokenizer = transformers.AutoTokenizer.from_pretrained(args.model)

    if torch.cuda.is_available():  # for nvidia GPUs
        device = torch.device("cuda:0")
    elif (
        torch.backends.mps.is_available()
    ):  # for Apple Metal Performance Sharder (mps) GPUs
        device = torch.device("mps")
    else:
        device = torch.device("cpu")

    num_episodes = args.num_episodes
    num_features = config.hidden_size
    episode_length = args.num_steps
    agent = LlamaSAC(
        state_dims=config.hidden_size,
        hidden_dims=args.hidden,
        action_dims=env.action_space.n,
        gamma=args.gamma,
        alpha=args.alpha,
        rho=args.rho,
    ).to(device)
    optimizer_actor = optim.Adam(list(agent.policy_head.parameters()), args.policy_lr)
    optimizer_critics = optim.Adam(
        list(agent.qsrc1.parameters()) + list(agent.qsrc2.parameters()), args.q_lr
    )
    num_random = args.learning_starts
    emb_layer = args.emb_layer
    states = torch.zeros(num_episodes, args.num_steps, 2)
    rewards = torch.zeros(num_episodes)

    buffer = Buffer(buffer_size=args.buffer_size)

    states = torch.zeros(num_episodes, episode_length, 2)
    rewards = torch.zeros(num_episodes)
    for episode in range(num_episodes):
        obs, info = env.reset()
        S = torch.zeros(episode_length, num_features)
        A = torch.zeros(episode_length, agent.action_dims)
        S_prime = torch.zeros_like(S)
        R = torch.zeros(episode_length, 1)
        terminal = torch.zeros(episode_length, 1)
        terminated, truncated, step = 0, 0, 0
        while not (terminated or truncated):
            with torch.no_grad():
                text_features = get_embeddings(
                    obs["text"], emb_layer, model, tokenizer, device
                )
            S[step] = text_features.float()
            if episode < num_random:
                action = env.action_space.sample()
            else:
                action = agent.act(text_features).item()
            next_obs, r, terminated, truncated, info = env.step(action)
            with torch.no_grad():
                next_text_features = get_embeddings(
                    next_obs["text"], emb_layer, model, tokenizer, device
                )

            A[step] = torch.eye(agent.action_dims)[action]
            S_prime[step] = next_text_features.float()
            R[step] = r
            terminal[step] = torch.tensor([False]).float()

            obs = next_obs

            if episode >= 1:
                critic_loss = agent.train_critic(buffer)
                optimizer_critics.zero_grad()
                critic_loss.backward()
                optimizer_critics.step()
                if step % 2 == 0:
                    actor_loss = agent.train_actor()
                    optimizer_actor.zero_grad()
                    actor_loss.backward()
                    optimizer_actor.step()
                    agent.soft_update()

            step += 1
        buffer.append(S, A, S_prime, R, terminal)
        rewards[episode] = R.sum()
        if episode % 2 == 0:
            writer.add_scalar("charts/episodic_reward", R.sum().item(), episode)

        if episode > 2:
            writer.add_scalar("losses/critic_loss", critic_loss.item(), episode)
            writer.add_scalar("losses/actor_loss", actor_loss.item(), episode)

    writer.close()
