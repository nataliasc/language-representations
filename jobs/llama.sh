#!/bin/bash -l
#SBATCH -o $DIR_PATH/logs/job.out.%j
#SBATCH -e $DIR_PATH/logs/job.err.%j
#SBATCH --time=02:20:00
#SBATCH --constraint="gpu"
#SBATCH --gres=gpu:a100:4
#SBATCH --mem=480G
#SBATCH --cpus-per-task=64
#
#
# --- uncomment to use 2 GPUs on a shared node ---
# #SBATCH --gres=gpu:a100:2
# #SBATCH --cpus-per-task=36
# #SBATCH --mem=250000

cd $DIR_PATH

module purge
module load anaconda/3/2021.11
module load gcc/11 impi/2021.6
module load cuda/11.6
module load pytorch_distributed/gpu-cuda-11.6/1.13.0
pip3 install --user fire sentencepiece ipdb accelerate tqdm

export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
python3 solve_gridworld_llama2.py
