"""
Save a dataset of gridworld state description
embeddings extracted from LLaMA 2 Chat
"""
import argparse
import numpy as np
import time
import pandas as pd
import transformers
import torch
import gymnasium as gym
import gridworld
import numpy as np


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--num-episodes",
        type=int,
        default=1,
        help="Number of episodes to complete in the gridworld env",
    )
    parser.add_argument(
        "--emb-layer",
        type=int,
        default=-1,
        help="LLaMA layer to extract embeddings from",
    )
    parser.add_argument(
        "--model",
        type=str,
        default="meta-llama/Llama-2-7b-chat-hf",
        help="LLaMA model to use",
    )
    args = parser.parse_args()
    return args


# def cosine_similarity(a, b):
#     return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


def get_embeddings(text_inputs, emb_layer, model, tokenizer, device):
    with torch.no_grad():
        inputs = tokenizer(text_inputs, return_tensors="pt")
        text_features = (
            model(
                inputs.input_ids.to(device),
                return_dict=True,
                output_hidden_states=True,
            )
            .hidden_states[emb_layer][:, -1, :]
            .to("cpu")
        )
        text_features = text_features.to(torch.float32).squeeze()
        text_features = text_features.detach().numpy().tolist()
    return text_features


if __name__ == "__main__":
    args = parse_args()

    columns = {
        "x_state": [],
        "y_state": [],
        "x_target": [],
        "y_target": [],
        "prompt": [],
        "embedding": [],
    }

    df = pd.DataFrame(columns)
    df["embedding"] = df["embedding"].astype(object)
    env = gym.make("Gridworld", size=5, distance=3)
    prompts, x_states, y_states, x_target, y_target = [], [], [], [], []
    observation, _ = env.reset()
    for i in range(args.num_episodes):
        observation, _ = env.reset()
        terminated, truncated = 0, 0
        while not (terminated or truncated):
            prompts.append(observation["text"])
            x, y = observation["agent"]
            x_states.append(x)
            y_states.append(y)
            x_t, y_t = observation["target"]
            x_target.append(x_t)
            y_target.append(y_t)

            action = np.random.randint(4)
            observation, reward, terminated, truncated, info = env.step(action)

    print(f"Prompts collected: {len(prompts)}")
    df.x_state = x_states
    df.y_state = y_states
    df.x_target = x_target
    df.y_target = y_target
    df.prompt = prompts

    model_name = args.model
    config = transformers.AutoConfig.from_pretrained(model_name)
    model = transformers.AutoModelForCausalLM.from_pretrained(
        model_name,
        config=config,
        torch_dtype=torch.bfloat16,
        device_map="auto",
        use_flash_attention_2=True,
    )
    tokenizer = transformers.AutoTokenizer.from_pretrained(model_name)

    if torch.cuda.is_available():  # for nvidia GPUs
        device = torch.device("cuda:0")
    elif (
        torch.backends.mps.is_available()
    ):  # for Apple Metal Performance Sharder (mps) GPUs
        device = torch.device("mps")
    else:
        device = torch.device("cpu")

    df["embedding"] = df.prompt.apply(
        lambda x: get_embeddings(
            x, emb_layer=args.emb_layer, model=model, tokenizer=tokenizer, device=device
        )
    )
    df.to_csv(f"llama_embeddings_{time.strftime('%Y-%m-%d_%H-%M-%S')}.csv", index=False)
