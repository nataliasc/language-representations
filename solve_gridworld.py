"""Use LLaMA 1 to solve gridworld in token space"""
from inference import LLaMAInference
import gymnasium as gym
import gridworld
import os
import time
import torch
import pandas as pd
from itertools import product
import argparse
import random
import numpy as np
from enum import Enum


def act_llama(text, temp=0.0, max_tokens=3):
    raw_response = llama.generate(
        [text], temperature=temp, top_p=1, max_length=max_tokens
    )[0][0][len(text) :]
    return raw_response


def word_to_action(word):
    word = word.lower()
    if "right" in word:
        action = 0
    elif "up" in word:
        action = 1
    elif "left" in word:
        action = 2
    elif "down" in word:
        action = 3
    else:
        action = "Illegal"
    return action


class Actions(Enum):
    right = 0
    up = 1
    left = 2
    down = 3


action_to_direction = {
    Actions.right.value: np.array([1, 0]),
    Actions.up.value: np.array([0, 1]),
    Actions.left.value: np.array([-1, 0]),
    Actions.down.value: np.array([0, -1]),
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--gridworld-size",
        type=int,
        default=5,
        help="Size of the gridworld",
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    run_name = f"gridworld_{time.strftime('%Y-%m-%d_%H-%M-%S')}"
    llama_dir = os.getenv("LLAMA_DIR")
    episode_length = 15

    combinations = []
    starting_states = product(range(args.gridworld_size), repeat=2)
    goal_states = product(range(args.gridworld_size), repeat=2)

    for start, goal in product(starting_states, goal_states):
        if start != goal:
            combinations.append((start, goal))

    columns = {
        "x_starting_pos": [],
        "y_starting_pos": [],
        "x_goal": [],
        "y_goal": [],
        "goal_reached": [],
        "n_steps": [],
        "optimal": [],
    }
    df = pd.DataFrame(columns, dtype=object)
    llama = LLaMAInference(llama_dir, "65B", max_batch_size=1)

    for i in range(len(combinations)):
        goal, starting_position = combinations[i]
        env = gym.make(
                "Gridworld-v0",
                size=args.gridworld_size,
                goal=goal,
                starting_position=starting_position,
            )
        episode_length = 15

        obs, info = env.reset()
        states = np.array(obs["agent"])
        step, terminated, truncated, reward = 0, 0, 0, 0
        step_budget = episode_length
        prompt = ""
        instructions = (
            f"You are in a gridworld of size five by five, "
            f"where your position will always be given as (x, y) coordinates. "
            f"Both coordinates are integers between 1 and 5. "
            f"The square where x = 1 and y = 1 is in the bottom "
            f"left corner of the grid. Moving left decrements the x "
            f"coordinate of your position by 1. Moving right increments "
            f"the x coordinate by 1. Moving down or up decrements or increments "
            f"respectively the y coordinate by 1. Going beyond 1 and 5 is not possible. "
            f"Your goal is to get to the square with coordinates ({obs['target'][0] + 1}, "
            f"{obs['target'][1] + 1}) in as few steps as possible. You can take 15 steps "
            f"before the game terminates. When you answer, think one step at a time, and "
            f"give your answer in the form 'Move <direction>' where direction can only be "
            f"'left', 'right', 'up' or 'down.'"
        )
        prompt += instructions

        if (
            obs["agent"][0] == 0
            and obs["agent"][1] == 0
            or obs["agent"][0] == 0
            and obs["agent"][1] == args.gridworld_size - 1
            or obs["agent"][0] == args.gridworld_size - 1
            and obs["agent"][1] == 0
            or obs["agent"][0] == args.gridworld_size - 1
            and obs["agent"][1] == args.gridworld_size - 1
        ):
            prompt += (
                f"You are currently in the square with coordinates "
                f"(x, y) = ({obs['agent'][0] + 1}, {obs['agent'][1] + 1}). "
            )
        else:
            if (
                obs["target"][0] < obs["agent"][0]
                and obs["agent"][0] + 2 <= args.gridworld_size
            ):
                prompt += f"You are currently in the square with coordinates (x, y) = ({obs['agent'][0] + 2}, {obs['agent'][1] + 1}). What move will you take next in order to get to the goal?\nA: Move left\nQ: You are currently in the square with coordinates (x, y) = ({obs['agent'][0] + 1}, {obs['agent'][1] + 1}). You have {step_budget} steps left. "

            elif obs["target"][0] > obs["agent"][0] and obs["agent"][0] > 0:
                prompt += f"You are currently in the square with coordinates (x, y) = ({obs['agent'][0]}, {obs['agent'][1] + 1}). What move will you take next in order to get to the goal?\nA: Move right\nQ: You are currently in the square with coordinates (x, y) = ({obs['agent'][0] + 1}, {obs['agent'][1] + 1}). You have {step_budget} steps left. "

            elif (
                obs["target"][1] < obs["agent"][1]
                and obs["agent"][1] + 2 <= args.gridworld_size
            ):
                prompt += f"You are currently in the square with coordinates (x, y) = ({obs['agent'][0] + 1}, {obs['agent'][1] + 2}). What move will you take next in order to get to the goal?\nA: Move down\nQ: You are currently in the square with coordinates (x, y) = ({obs['agent'][0] + 1}, {obs['agent'][1] + 1}). Your goal is to get to the square with coordinates ({obs['target'][0] + 1}, {obs['target'][1] + 1}). You have {step_budget} steps left. "

            elif obs["target"][1] > obs["agent"][1] and obs["agent"][1] > 0:
                prompt += f"You are currently in the square with coordinates (x, y) = ({obs['agent'][0] + 1}, {obs['agent'][1]}). What move will you take next in order to get to the goal?\nA: Move up\nQ: You are currently in the square with coordinates (x, y) = ({obs['agent'][0] + 1}, {obs['agent'][1] + 1}). You have {step_budget} steps left. "

        response = act_llama(
            prompt
            + f"What move will you take next in order to get to the square with coordinates ({obs['target'][0] + 1}, {obs['target'][1] + 1})?\nA: Move",
        )
        df.at[i, f"prompt_step_{step}"] = (
            prompt
            + f"What move will you take next in order to get to the square with coordinates ({obs['target'][0] + 1}, {obs['target'][1] + 1})?\nA: Move"
        )
        df.at[i, f"response_step_{step}"] = response
        while not (terminated or truncated) and step < episode_length:
            step += 1
            action = word_to_action(response)
            step_budget -= 1
            if action != "Illegal":
                prompt += f"What move will you take next?\nA: Move {response}\n"
                direction = action_to_direction[action]
                newpos = obs["agent"] + direction
                if (newpos < 0).any() or (newpos >= args.gridworld_size).any():
                    prompt += f"Q: You have just tried walking through a wall. "
                    prompt += (
                        f"You are currently in the square with coordinates "
                        f"({obs['agent'][0] + 1}, {obs['agent'][1] + 1}). Your goal is to get "
                        f"to the square with coordinates ({obs['target'][0] + 1}, {obs['target'][1] + 1}). "
                    )
                    response = act_llama(
                        prompt + f"\nA: Move",
                    )
                    df.at[i, f"prompt_step_{step}"] = prompt + f"\nA: Move"
                else:
                    obs, r, terminated, truncated, info = env.step(action)
                    reward += r
                    if (obs["agent"] == obs["target"]).all():
                        prompt += f"Q: Congratulations! You have reached the target!"
                        df.at[i, f"prompt_step_{step}"] = prompt
                    else:
                        prompt += (
                            f"Q: You are currently in the square with coordinates "
                            f"({obs['agent'][0] + 1}, {obs['agent'][1] + 1}). "
                        )

                        response = act_llama(
                            prompt
                            + (
                                f"What move will you take "
                                f"next in order to get to the square with "
                                f"coordinates ({obs['target'][0] + 1}, {obs['target'][1] + 1})?\nA: Move"
                            ),
                        )
                        df.at[i, f"prompt_step_{step}"] = prompt + (
                            f"What move will you take "
                            f"next in order to get to the square with "
                            f"coordinates ({obs['target'][0] + 1}, {obs['target'][1] + 1})?\nA: Move"
                        )
            else:
                illegal_move = (
                    f"Think one step at a time and give your answer "
                    f"in the form 'Move <direction>' where direction can only be 'left', "
                    f"'right', 'up' or 'down'. "
                    f"Your goal is to get to the square with coordinates ({obs['target'][0] + 1}, "
                    f"{obs['target'][1] + 1}).\nWhat move will you take next?"
                )
                response = act_llama(
                    prompt + illegal_move + f"\nA: Move",
                )
                df.at[i, f"prompt_step_{step}"] = (
                    prompt + illegal_move + f"\nA: Move"
                )

            df.at[i, f"response_step_{step}"] = response
            states = np.vstack((states, obs["agent"]))
        print("Steps taken: ", step)
        print("\nEpisode reward: ", reward, "\n\n")
        print(states)
        df.at[i, "x_goal"] = goal[0]
        df.at[i, "y_goal"] = goal[1]
        df.at[i, "x_starting_pos"] = starting_position[0]
        df.at[i, "y_starting_pos"] = starting_position[1]
        for j, state in enumerate(states):
            x, y = state
            df.at[i, f"x_step_{j}"] = x
            df.at[i, f"y_step_{j}"] = y
        # last step
        x, y = states[-1]
        df.at[i, "x_last_step"] = x
        df.at[i, "y_last_step"] = y
        df["optimal"] = abs(df["x_goal"] - df["x_starting_pos"]) + abs(
            df["y_goal"] - df["y_starting_pos"]
        )
        df.at[i, "n_steps"] = len(states)

    df.to_csv(f"{run_name}.csv", index=False)
