""""Adapted from
https://github.com/tankred-saanum/llamarl/blob/master/agents/replay_buffer.py"""
import torch

class Buffer():
    def __init__(self, buffer_size =1_000_000, batch_size=24):
        self.data = None
        self.buffer_size = buffer_size
        self.buffer_is_full = False
        self.batch_size = batch_size
        self.names = ['S', 'A', 'S_prime', 'R', 'terminal']

    def append(self, s, a, s_prime, r, terminal):
        _data = [s, a, s_prime, r, terminal]
        if type(self.data) == type(None):
            self.data = {}
            for i, (name, dat) in enumerate(zip(self.names, _data)):
                self.data[name] = dat

        else:
            for i, (name, dat) in enumerate(zip(self.names, _data)):
                self.data[name] = torch.cat((self.data[name], dat), dim= 0)


            if self.buffer_is_full:
                for i, (name, dat) in enumerate(zip(self.names, _data)):
                    self.data[name] = self.data[name][len(s):]

            else:
                self.buffer_is_full = (len(self.data['S']) >= self.buffer_size)

    def sample(self):
        idx = torch.randint(len(self.data['S']), (self.batch_size, ))
        data = (self.data[name][idx] for name in self.names)
        return data