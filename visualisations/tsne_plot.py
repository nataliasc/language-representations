import pandas as pd
import numpy as np
from ast import literal_eval
import matplotlib.pyplot as plt
import matplotlib
from matplotlib_inline.backend_inline import set_matplotlib_formats
from sklearn.manifold import TSNE
import seaborn as sns
from ast import literal_eval

set_matplotlib_formats("retina")

embeddings_data = pd.read_csv("./data/llama_embeddings.csv", sep=",")
embeddings_data["embedding"] = embeddings_data.embedding.apply(literal_eval).apply(np.array)

embeddings_data.y_state = embeddings_data.y_state + 1
embeddings_data.x_state = embeddings_data.x_state + 1
state_cols = ["x_state", "y_state"]
embeddings_data["x, y state"] = embeddings_data[state_cols].apply(
    lambda row: ", ".join(row.values.astype(str)), axis=1
)

state_counts = embeddings_data["x, y state"].value_counts()
plt.figure(figsize=(12, 3))
sns.barplot(x=state_counts.index, y=state_counts.values, color="#001260", alpha=0.5)
sns.despine()
plt.tight_layout(pad=0.05)
plt.xlabel("($x$, $y$) coordinates of the current state")
plt.ylabel("Count")
plt.savefig("states.pdf")

matrix = np.array(embeddings_data.embedding.to_list())

tsne = TSNE(
    n_components=2,
    perplexity=200,
    random_state=2,
    init="random",
    learning_rate=200,
    n_iter=30_000,
)
plot_data = tsne.fit_transform(matrix)

# colors from 10.5281/zenodo.1243909
y_colors = ["#001260", "#2F7CA5", "#EBE5E0", "#C27142", "#590007"]
x = [x for x, y in plot_data]
y = [y for x, y in plot_data]

color_to_state = {
    state: color
    for state, color in zip(sorted(set(embeddings_data["y_state"].values)), y_colors)
}

embeddings_data["y_color"] = embeddings_data.apply(
    lambda row: color_to_state[row["y_state"]], axis=1
)

# t-SNE plot for the y coordinate
plt.figure(figsize=(8, 5))
plt.scatter(x, y, c=embeddings_data.y_color, alpha=0.3)
legend_labels = sorted(list(set(embeddings_data["y_state"])))
legend_handles = [
    plt.Line2D(
        [0], [0], marker="o", color="w", markerfacecolor=y_colors[i], markersize=10
    )
    for i in range(len(set(embeddings_data["y_state"])))
]

plt.legend(
    legend_handles,
    legend_labels,
    title="$y$ coordinate",
    loc="upper left",
    ncols=2,
)

plt.title("State description embeddings visualized using t-SNE")
plt.xlabel("t-SNE Dimension 1")
plt.ylabel("t-SNE Dimension 2")
plt.tight_layout(pad=0.05)
sns.despine()
plt.savefig("tsne_last_layer_y.pdf")


# t-SNE plot for the (x,y) tuple of the current state
colors = [
    "#001260",
    "#01226B",
    "#013376",
    "#034380",
    "#06558B",
    "#156798",
    "#2F7CA5",
    "#4F92B4",
    "#71A7C4",
    "#92BDD1",
    "#B3D1DF",
    "#D4E2E9",
    "#EBE5E0",
    "#ECD4C7",
    "#E4BFAA",
    "#DBAB8E",
    "#D29773",
    "#CB835B",
    "#C27142",
    "#BA5E2A",
    "#AA4613",
    "#932E05",
    "#7E1D05",
    "#6B0D06",
    "#590007",
]
color_to_state = {
    state: color
    for state, color in zip(sorted(set(embeddings_data["x, y state"].values)), colors)
}


embeddings_data["state_color"] = embeddings_data.apply(
    lambda row: color_to_state[row["x, y state"]], axis=1
)
colormap = matplotlib.colors.ListedColormap(colors)

plt.figure(figsize=(8.5, 4))
plt.scatter(x, y, c=embeddings_data.state_color, alpha=0.3)

legend_labels = sorted(list(set(embeddings_data["x, y state"])))
legend_handles = [
    plt.Line2D(
        [0],
        [0],
        marker="o",
        color="w",
        markerfacecolor=colormap.colors[i],
        markersize=10,
    )
    for i in range(25)
]

plt.legend(
    legend_handles,
    legend_labels,
    title="Current state ($x$, $y$) coordinates",
    loc="center left",
    bbox_to_anchor=(1, 0.5),
    ncol=2,
)
plt.title("State description embeddings visualized using t-SNE")
plt.xlabel("t-SNE Dimension 1")
plt.ylabel("t-SNE Dimension 2")
plt.tight_layout(pad=0.05)
sns.despine()
plt.savefig("tsne_last_layer_state_embeddings.pdf")
