#!/usr/bin/env Rscript

library(tidyverse)
library(scico)

l <- read_csv("llama2.csv")
l$model <- "LLaMA 2 70B"
l_c2 <- read_csv("llama2_chat.csv")
l_c2$model <- "LLaMA 2 70B Chat"
l1 <- read_csv("llama1.csv")
l1$n_steps <- l1$n_steps - 1
l1$model <- "LLaMA 1 65B"
r <- read_csv("random-agent.csv")
r$model <- "Random agent"
l <- select(l, !starts_with("prompt"))
l1 <- select(l1, !starts_with("prompt"))
l_c2 <- select(l_c2, !starts_with("prompt"))
l <- select(l, !starts_with("response"))
l1 <- select(l1, !starts_with("response"))
l_c2 <- select(l_c2, !starts_with("response"))
gridworld_results <- rbind(l, l1, r, l_c2)

gridworld_results$goal_reached <- (gridworld_results$x_goal == gridworld_results$x_last_step) & (gridworld_results$y_goal == gridworld_results$y_last_step)
gridworld_results <- gridworld_results |> mutate(
  solved = case_when(
    goal_reached == T ~ "Solved",
    goal_reached == F ~ "Did not solve"
  ),
  solution = case_when(
    n_steps == optimal ~ "Optimal",
    goal_reached == T ~ "Nonoptimal",
    .default = "Did not solve"
  )
)

gridworld_results |> 
  group_by(model, optimal) |> 
  summarise(prob = mean(goal_reached)) |> 
  ungroup() |> 
  mutate(model = fct_relevel(model,  "LLaMA 2 70B Chat",
                             "LLaMA 1 65B", "LLaMA 2 70B",
                             "Random agent")) |> 
  ggplot(aes(optimal, prob, colour=model, shape=model)) + 
  geom_point(size = 2.5) +
  geom_line() +
  labs(x = "Distance to goal (steps)",
       y = "Probability of reaching the goal",
       colour = "",
       shape = "") +
  theme_minimal(base_size = 15) +
  scale_colour_scico_d(palette = 'vik') +
  theme(text = element_text(size = 15, family = "Minion Pro"),
        legend.position=c(.8,.95))

ggsave("images/model-probs.pdf", device=cairo_pdf)

gridworld_results |> 
  filter(goal_reached == T) |> 
  group_by(model, optimal) |> 
  summarise(diff = mean(n_steps - optimal)) |> 
  ungroup() |> 
  mutate(model = fct_relevel(model,  "LLaMA 2 70B Chat",
                             "LLaMA 1 65B", "LLaMA 2 70B",
                             "Random agent")) |> 
  ggplot(aes(optimal, diff, colour=model, shape=model)) + 
  geom_point(size = 2.5) +
  geom_line() +
  theme_minimal(base_size = 15) +
  scale_colour_scico_d(palette = 'vik') +
  scale_y_continuous(breaks=seq(0, 9, 1)) +
  labs(x = "Distance to goal (steps)",
       y = "Mean difference from optimal solution (steps)",
       colour = "",
       shape = "") +
  scale_fill_scico_d(palette = 'glasgow', name = "Solution") +
  theme(text = element_text(size = 15, family = "Minion Pro"),
        strip.text = element_text(size = 15, family = "Minion Pro"))