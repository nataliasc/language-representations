import torch
import torch.optim as optim
import transformers
import argparse
from rb import Buffer
from distutils.util import strtobool
from sac import LlamaSAC
import gymnasium as gym
import gridworld
import optuna

def get_embeddings(text_inputs, emb_layer, model, tokenizer, device):
    inputs = tokenizer(text_inputs, return_tensors="pt")
    text_features = (
        model(
            inputs.input_ids.to(device),
            return_dict=True,
            output_hidden_states=True,
        )
        .hidden_states[-(emb_layer)][:, -1, :]
        .detach()
        .cpu()
    )
    text_features = text_features.to(torch.float32)
    return text_features

def train_fn(trial):
    # Hyperparameters
    lr = trial.suggest_float('lr', 1e-4, 1, log=True)
    hidden = trial.suggest_int('hidden', 256, 512)
    bs = trial.suggest_int('batch size', 12, 64)
    # num_random = trial.suggest_int('random episodes', 25, 250)
    num_random = 150
    
    # Model and training configuration
    env = gym.make(
            "Gridworld-v0",
            size=5,
            goal=(2, 3),
            starting_position=(0, 0),
        )
    if torch.cuda.is_available():  # for nvidia GPUs
        device = torch.device('cuda:0')
    elif torch.backends.mps.is_available(): # for Apple Metal Performance Sharder (mps) GPUs
        device = torch.device('mps')
    else:
        device = torch.device('cpu')
    num_episodes = 700
    num_features = 8192
    episode_length = env.num_steps
    agent = LlamaSAC(
        state_dims=config.hidden_size, hidden_dims=hidden, action_dims=4, gamma=0.99, alpha=0.1, rho=0.01
    ).to(device)
    optimizer_actor = optim.Adam(list(agent.policy_head.parameters()), lr)
    optimizer_critics = optim.Adam(
        list(agent.qsrc1.parameters()) + list(agent.qsrc2.parameters()), lr
    )
    rewards = torch.zeros(num_episodes)

    buffer = Buffer(batch_size=bs)
    emb_layer = -1

    # Training loop
    for episode in range(num_episodes):
        obs, info = env.reset()
        S = torch.zeros(episode_length, num_features)
        A = torch.zeros(episode_length, agent.action_dims)
        S_prime = torch.zeros_like(S)
        R = torch.zeros(episode_length, 1)
        terminal = torch.zeros(episode_length, 1)
        terminated, truncated, step = 0, 0, 0
        while not (terminated or truncated):
            with torch.no_grad():
                text_features = get_embeddings(
                    obs["text"], emb_layer, model, tokenizer, device
                )
            S[step] = text_features.float()
            if episode < num_random:
                action = env.action_space.sample()
            else:
                action = agent.act(text_features).item()
            next_obs, r, terminated, truncated, info = env.step(action)
            with torch.no_grad():
                next_text_features = get_embeddings(
                    next_obs["text"], emb_layer, model, tokenizer, device
                )


            A[step] = torch.eye(agent.action_dims)[action]
            S_prime[step] = next_text_features.float()
            R[step] = r
            terminal[step] = torch.tensor([False]).float()

            obs = next_obs

            if episode >= 1:
                critic_loss = agent.train_critic(buffer)
                optimizer_critics.zero_grad()
                critic_loss.backward()
                optimizer_critics.step()
                if step % 2 == 0:
                    actor_loss = agent.train_actor()
                    optimizer_actor.zero_grad()
                    actor_loss.backward()
                    optimizer_actor.step()
                    agent.soft_update()

            step += 1
        buffer.append(S, A, S_prime, R, terminal)
        rewards[episode] = R.sum()

    return rewards.mean().item()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n-trials', type=int, default=20,
                        help='Number of trials to run')

    args = parser.parse_args()
    model_name = "meta-llama/Llama-2-70b-chat-hf"
    config = transformers.AutoConfig.from_pretrained(model_name)
    model = transformers.AutoModelForCausalLM.from_pretrained(model_name,
                                                                config=config,
                                                                torch_dtype=torch.bfloat16,
                                                                device_map="auto",
                                                                use_flash_attention_2=True,
                                                                )
    tokenizer = transformers.AutoTokenizer.from_pretrained(model_name)

    study = optuna.create_study(direction='maximize',
                            storage="sqlite:///optuna/gridworld-llama-sac.db",
                            study_name="HP search",
                            load_if_exists=True)
    study.optimize(train_fn, n_trials=args.n_trials)
