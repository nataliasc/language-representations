## Installation

To install the Gridworld environment, run the following commands:

```{shell}
cd gridworld
pip install -e .
```
## Basic usage

```{python}
import gymnasium as gym
import gridworld

env = gym.make("Gridworld")
observation, info = env.reset()

```
