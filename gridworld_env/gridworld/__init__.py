from gymnasium.envs.registration import register

register(
    id="Gridworld-v0",
    entry_point="gridworld.envs:Gridworld",
    max_episode_steps=15,
)
