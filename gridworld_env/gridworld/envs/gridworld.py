from enum import Enum
import gymnasium as gym
from gymnasium import spaces
import numpy as np
from itertools import product
import random
import string


B_INST, E_INST = "[INST]", "[/INST]"
B_SYS, E_SYS = "<<SYS>>\n", "\n<</SYS>>\n\n"


class Actions(Enum):
    right = 0
    up = 1
    left = 2
    down = 3


class Gridworld(gym.Env):
    metadata = {"render_modes": []}

    def __init__(self, render_mode=None, size=5, distance=None, goal=None, starting_position=None):
        if (distance and goal):
            raise ValueError('Provide either goal and starting position OR distance')
        self.size = size  # Size of the square grid
        self.budget = 15 # Counter for steps in the env
        self.random_tiles = np.random.uniform(0, 1, [self.size, self.size, 8192])
        if distance:
            self.distance = distance  # Distance from start to goal
            self.envs = []

            for start, target in product(
                product(range(self.size), repeat=2), product(range(self.size), repeat=2)
            ):
                d = abs(target[0] - start[0]) + abs(target[1] - start[1])
                if start != target and d == self.distance:
                    self.envs.append(np.array([start, target]))
        elif goal:
            self.distance = None
            self._target_location = np.array(goal)
            self._agent_location = np.array(starting_position)

        # Observations are dictionaries with the agent's and the target's
        # location; textual description that can be used as LLaMA 2 Chat prompt;
        # random vectors for the random baseline

        self.observation_space = spaces.Dict(
            {
                "agent": spaces.Box(0, size - 1, shape=(2,), dtype=int),
                "target": spaces.Box(0, size - 1, shape=(2,), dtype=int),
                "text": spaces.Text(
                    min_length=1,
                    max_length=100_000,
                    charset=string.printable,
                ),
                "random": spaces.Box(low=0, high=1.0, shape=(8192,), dtype=np.float64)
            }
        )

        # 4 actions: "right", "up", "left", "down"
        self.action_space = spaces.Discrete(4)

        # Map action indices to directions

        self._action_to_direction = {
            Actions.right.value: np.array([1, 0]),
            Actions.up.value: np.array([0, 1]),
            Actions.left.value: np.array([-1, 0]),
            Actions.down.value: np.array([0, -1]),
        }
        self._action_to_string = {
            Actions.right.value: "right",
            Actions.up.value: "up",
            Actions.left.value: "left",
            Actions.down.value: "down",
        }

        assert render_mode is None or render_mode in self.metadata["render_modes"]
        self.render_mode = render_mode

        """
        If human-rendering is used, `self.window` will be a reference
        to the window that we draw to. `self.clock` will be a clock that is used
        to ensure that the environment is rendered at the correct framerate in
        human-mode. They will remain `None` until human-mode is used for the
        first time.
        """
        self.window = None
        self.clock = None

    def _get_obs(self):
        return {
            "agent": self._agent_location,
            "target": self._target_location,
            "text": self._prompt,
            "random": self._random_tile,
        }

    def _get_info(self):
        return {
            "distance": np.linalg.norm(
                self._agent_location - self._target_location, ord=1
            )
        }

    def reset(self, seed=None, options=None):
        # Seed self.np_random
        super().reset(seed=seed)

        # Sample starting position and target position randomly
        # with the constraint that the target is `distance`
        # steps away and distinct from starting position
        if self.distance:
            env = random.choice(self.envs)
            self._agent_location, self._target_location = env[0], env[1]
        
        self._random_tile = self.random_tiles[self._agent_location[0], self._agent_location[1]]

        self._prompt = f"{B_INST} {B_SYS}Your task is to navigate a simple grid of size five by five. Your position on the grid will always be given to you as (x, y) coordinates. Both coordinates are integers between 1 and 5. The square where x = 1 and y = 1 is in the bottom left corner of the grid. Moving left decrements the x coordinate of your position by 1. Moving right increments the x coordinate by 1. Moving down or up decrements or increments respectively the y coordinate by 1. Going beyond 1 and 5 is not possible. Your goal is to get to the square with coordinates ({self._target_location[0] + 1}, {self._target_location[1] + 1}) in as few steps as possible. You have {self.budget + 1} steps before the session terminates. When you answer, think one step at a time, and give your answer in the form 'Move <direction>' where direction can only be 'left', 'right', 'up' or 'down'. You can move one square at a time.{E_SYS}"
        if (
            (self._agent_location == [0, 0]).all()
            or (self._agent_location == [0, self.size - 1]).all()
            or (self._agent_location == [self.size - 1, 0]).all()
            or (self._agent_location == (self.size - 1, self.size - 1)).all()
        ):
            self._prompt += f"You are currently in the square with coordinates (x, y) = ({self._agent_location[0] + 1}, {self._agent_location[1] + 1}). "
        else:
            if self._target_location[0] < self._agent_location[0] and self._agent_location[0] + 2 <= self.size:

                self._prompt += f"You are currently in the square with coordinates (x, y) = ({self._agent_location[0] + 2}, {self._agent_location[1] + 1}). What move will you take next in order to get to the goal? {E_INST}\nMove left\n{B_INST} You are currently in the square with coordinates (x, y) = ({self._agent_location[0] + 1}, {self._agent_location[1] + 1}). You have {self.budget} steps left. "

            elif self._target_location[0] > self._agent_location[0] and self._agent_location[0] > 0:

                self._prompt += f"You are currently in the square with coordinates (x, y) = ({self._agent_location[0]}, {self._agent_location[1] + 1}). What move will you take next in order to get to the goal? {E_INST}\nMove right\n{B_INST} You are currently in the square with coordinates (x, y) = ({self._agent_location[0] + 1}, {self._agent_location[1] + 1}). You have {self.budget} steps left. "

            elif self._target_location[1] < self._agent_location[1] and self._agent_location[1] + 2 <= self.size:

                self._prompt += f"You are currently in the square with coordinates (x, y) = ({self._agent_location[0] + 1}, {self._agent_location[1] + 2}). What move will you take next in order to get to the goal? {E_INST}\nMove down\n{B_INST} You are currently in the square with coordinates (x, y) = ({self._agent_location[0] + 1}, {self._agent_location[1] + 1}). Your goal is to get to the square with coordinates ({self._target_location[0] + 1}, {self._target_location[1] + 1}). You have {self.budget} steps left. "

            elif self._target_location[1] > self._agent_location[1] and self._agent_location[1] + 2 <= self.size:

                self._prompt += f"You are currently in the square with coordinates (x, y) = ({self._agent_location[0] + 1}, {self._agent_location[1]}). What move will you take next in order to get to the goal? {E_INST}\nMove up\n{B_INST} You are currently in the square with coordinates (x, y) = ({self._agent_location[0] + 1}, {self._agent_location[1] + 1}). You have {self.budget} steps left. "

        observation = self._get_obs()
        observation["text"] += f"What move will you take next in order to get to the goal? {E_INST}\nMove"
        info = self._get_info()

        if self.render_mode == "human":
            self._render_frame()

        return observation, info

    def step(self, action):
        self.budget -= 1
        # Map action index to direction
        direction = self._action_to_direction[action]
        self._prompt += f"What move will you take next? {E_INST}\nMove {self._action_to_string[action]}\n"

        if (
            (0 <= self._agent_location + direction)
            & (self._agent_location + direction < self.size)
        ).all():
            self._agent_location += direction
            self._random_tile = self.random_tiles[self._agent_location[0], self._agent_location[1]]
            terminated = np.array_equal(self._agent_location, self._target_location)
            if terminated:
                self._prompt += (
                    f"{B_INST} Congratulations! You have reached the goal! {E_INST}"
                )
                reward = 3
                observation = self._get_obs()
            else:
                self._prompt += f"{B_INST} You are currently in the square with coordinates ({self._agent_location[0] + 1}, {self._agent_location[1] + 1}). You have {self.budget} steps left. "
                reward = -1
                observation = self._get_obs()
                observation[
                    "text"
                ] += f"What move will you take next in order to get to the square with coordinates ({self._agent_location[0] + 1}, {self._agent_location[1] + 1})? {E_INST}\nMove"

        else:
            terminated = False
            reward = -1
            self._prompt += f"{B_INST} You have just tried walking through a wall. "
            self._prompt += f"You are currently in the square with coordinates ({self._agent_location[0] + 1}, {self._agent_location[1] + 1}). Your goal is to get to the square with coordinates ({self._target_location[0] + 1}, {self._target_location[1] + 1}) in as few steps as possible. You have {self.budget} steps left. "
            observation = self._get_obs()
            observation["text"] += f"What move will you take next? {E_INST}\nMove"

        info = self._get_info()

        return observation, reward, terminated, False, info
