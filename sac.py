"""SAC algorithm implementation adapted from
Tankred Saanum's implementation
https://github.com/tankred-saanum/llamarl"""
import torch
from torch import nn


class IdentityEncoder(nn.Module):
    def __init__(self):
        super(IdentityEncoder, self).__init__()
        self.im_size = 0

    def forward(self, x):
        return x


class Actor(nn.Module):
    def __init__(self, state_dims, action_dims, hidden_dims=256, discrete=False):
        super(Actor, self).__init__()
        self.action_dims = action_dims
        self.discrete = discrete
        if self.discrete:
            self.policy_head = nn.Sequential(
                nn.Linear(state_dims, hidden_dims),
                nn.ReLU(),
                nn.Linear(hidden_dims, hidden_dims),
                nn.ReLU(),
                nn.Linear(hidden_dims, action_dims),
                nn.Softmax(),
            )
        else:
            self.policy_head = nn.Sequential(
                nn.Linear(state_dims, hidden_dims),
                nn.ReLU(),
                nn.Linear(hidden_dims, hidden_dims),
                nn.ReLU(),
                nn.Linear(hidden_dims, action_dims),
            )

    def forward(self, s):
        return self.policy_head(s)


class Critic(nn.Module):
    def __init__(self, state_dims, action_dims, hidden_dims=256):
        super(Critic, self).__init__()
        self.action_dims = action_dims
        if torch.cuda.is_available():  # for nvidia GPUs
            self.device = torch.device("cuda:0")
        elif (torch.backends.mps.is_available()):  # for Apple Metal Performance Sharder (mps) GPUs
            self.device = torch.device("mps")
        else:
            self.device = torch.device("cpu")
        self.value_head = nn.Sequential(
            nn.Linear(state_dims + action_dims, hidden_dims),
            nn.ReLU(),
            nn.Linear(hidden_dims, hidden_dims),
            nn.ReLU(),
            nn.Linear(hidden_dims, 1),
        )

    def forward(self, s, a):
        sa = torch.cat((s, a), dim=-1)
        sa = sa.to(self.device)
        return self.value_head(sa)


class LinearActor(Actor):
    def __init__(self, state_dims, action_dims, hidden_dims=256, discrete=False):
        super(LinearActor, self).__init__(
            state_dims, action_dims, hidden_dims, discrete
        )
        if self.discrete:
            self.policy_head = nn.Sequential(nn.Linear(state_dims, action_dims), nn.Softmax())
        else:
            self.policy_head = nn.Sequential(nn.Linear(state_dims, action_dims))


class LinearCritic(Critic):
    def __init__(self, state_dims, action_dims, hidden_dims=256):
        super(LinearCritic, self).__init__(state_dims, action_dims, hidden_dims)

        self.value_head = nn.Sequential(nn.Linear(state_dims + action_dims, 1))


class SAC(nn.Module):
    def __init__(
        self, state_dims, action_dims, hidden_dims=30, gamma=0.99, alpha=0.1, rho=0.01
    ):
        super(SAC, self).__init__()
        self.state_dims = state_dims
        self.action_dims = action_dims
        self.gamma = gamma
        if torch.cuda.is_available():  # for nvidia GPUs
            self.device = torch.device("cuda:0")
        elif (torch.backends.mps.is_available()):  # for Apple Metal Performance Sharder (mps) GPUs
            self.device = torch.device("mps")
        else:
            self.device = torch.device("cpu")

        self.encoder = IdentityEncoder()

        self.policy_head = Actor(state_dims, action_dims, hidden_dims, discrete=True)

        self.qtarget1 = Critic(state_dims, action_dims, hidden_dims)
        self.qtarget2 = Critic(state_dims, action_dims, hidden_dims)

        self.qsrc1 = Critic(state_dims, action_dims, hidden_dims)
        self.qsrc2 = Critic(state_dims, action_dims, hidden_dims)

        self.target_models = [self.qtarget1, self.qtarget2]
        self.src_models = [self.qsrc1, self.qsrc2]

        self.alpha = torch.nn.Parameter(
            torch.tensor([alpha], device="cuda" if torch.cuda.is_available() else "cpu").log()
        )
        self.rho = rho
        self.action_indices = torch.arange(self.action_dims).to(device=self.device)
        self.action_vectors = torch.eye(self.action_dims).float().to(device=self.device)

    def forward(self, state):
        p = self.policy_head(state)
        policy = torch.distributions.Categorical(p)
        return policy

    def act(self, s):
        s = self.encoder(s)
        policy = self(s)
        return policy.sample()

    def act_deterministic(self, s):
        s = self.encoder(s)
        policy = self(s)
        return policy.probs.argmax(dim=-1)

    def concatenate_actions(self, s):
        """Takes a tensor of embedding vectors and adds all actions for every vector"""
        s_rep = torch.repeat_interleave(s, self.action_dims, dim=0)
        a_rep = self.action_vectors.repeat(s.shape[0], 1)

        return (
            torch.cat((s_rep, a_rep), dim=-1),
            s_rep.to(device=self.device),
            a_rep.to(device=self.device),
        )

    def get_action_probabilities(self, s):
        policy = self(s)
        # actions = policy.sample()
        action_probabilities = policy.probs
        z = action_probabilities == 0.0
        z = z.float() * 1e-8
        log_probs = torch.log(action_probabilities + z)

        return action_probabilities, log_probs

    def get_q_vals(self, s, target=True):
        sa, s_repeat, a_repeat = self.concatenate_actions(s)
        if target:
            q_vals1 = self.qtarget1(s_repeat, a_repeat).reshape(s.size(0), self.action_dims)
            q_vals2 = self.qtarget2(s_repeat, a_repeat).reshape(s.size(0), self.action_dims)
            q_vals = torch.min(q_vals1, q_vals2)
        else:
            q_vals1 = self.qsrc1(s_repeat, a_repeat).reshape(s.size(0), self.action_dims)
            q_vals2 = self.qsrc2(s_repeat, a_repeat).reshape(s.size(0), self.action_dims)
            q_vals = torch.min(q_vals1, q_vals2)
        return q_vals

    def train_critic(self, buffer):
        """Evaluates the state-action pairs of an episode.
        Returns the estimated value of each state and the
        log prob of the action taken
        """
        s, a, s_prime, r, terminal = buffer.sample()
        self.s = s
        s = self.encoder(s)
        s_prime = self.encoder(s_prime)
        with torch.no_grad():
            action_probs, log_probs = self.get_action_probabilities(
                s_prime
            )  # get all action probabilities and log probs
            q_prime = self.get_q_vals(s_prime, target=True)  # get all Q values
            q_prime = action_probs * (
                q_prime - self.alpha.exp().detach() * log_probs
            )  # compute expectation by weighing according to p
            q_prime = q_prime.sum(dim=1).unsqueeze(-1)  # integrate
            target = r + ((self.gamma * (1 - terminal)) * q_prime)

        q_vals1 = self.qsrc1(s, a)
        q_vals2 = self.qsrc2(s, a)

        crit = nn.MSELoss()
        loss1 = crit(q_vals1, target)
        loss2 = crit(q_vals2, target)

        return loss1 + loss2

    def train_actor(self):
        s = self.encoder(self.s)
        action_probs, log_probs = self.get_action_probabilities(
            s
        )  # get all action probabilities and log probs
        q_prime = self.get_q_vals(s, target=False)
        inside_term = self.alpha.exp().detach() * log_probs - q_prime
        loss = (action_probs * inside_term).sum(dim=1).mean()

        return loss

    def soft_update(self):
        """Updates the target network in the direction of the local network
        by taking a step size less than one so the target network's parameter
        values trail the local networks. This helps stabilise training
        """
        for target_model, local_model in zip(self.target_models, self.src_models):
            for target_param, local_param in zip(
                target_model.parameters(), local_model.parameters()
            ):
                target_param.data.copy_(
                    (self.rho * local_param.data)
                    + ((1.0 - self.rho) * target_param.data)
                )


class ClipAgent(nn.Module):
    def __init__(self, state_dims, action_dims=4, gamma=0.99):
        super(ClipAgent, self).__init__()
        if torch.cuda.is_available():  # for nvidia GPUs
            self.device = torch.device("cuda:0")
        elif (torch.backends.mps.is_available()):  # for Apple Metal Performance Sharder (mps) GPUs
            self.device = torch.device("mps")
        else:
            self.device = torch.device("cpu")
        self.action_dims = action_dims
        self.gamma = gamma
        self.state_dims = state_dims
        self.policy_head = nn.Linear(self.state_dims, self.action_dims)

    def forward(self, text_features):
        text_features = text_features.to(self.device)
        p = torch.softmax(self.policy_head(text_features), dim=-1)
        policy = torch.distributions.Categorical(p)
        return policy

    def act(self, text_features):
        policy = self(text_features)
        return policy.sample()

    def act_deterministic(self, text_features):
        policy = self(text_features)
        return policy.probs.argmax(dim=-1)


class LlamaSAC(ClipAgent):
    def __init__(
        self,
        state_dims,
        hidden_dims=512,
        action_dims=4,
        gamma=0.99,
        alpha=0.1,
        rho=0.01,
    ):
        super(LlamaSAC, self).__init__(state_dims, action_dims, gamma)

        self.policy_head = nn.Sequential(
            nn.Linear(state_dims, hidden_dims),
            nn.ReLU(),
            nn.Linear(hidden_dims, hidden_dims),
            nn.ReLU(),
            nn.Linear(hidden_dims, action_dims),
            nn.Softmax(),
        )

        self.qtarget1 = Critic(state_dims, action_dims, hidden_dims)
        self.qtarget2 = Critic(state_dims, action_dims, hidden_dims)

        self.qsrc1 = Critic(state_dims, action_dims, hidden_dims)
        self.qsrc2 = Critic(state_dims, action_dims, hidden_dims)

        self.target_models = [self.qtarget1, self.qtarget2]
        self.src_models = [self.qsrc1, self.qsrc2]

        self.alpha = torch.nn.Parameter(
            torch.tensor(
                [alpha], device=self.device
            ).log()
        )
        self.rho = rho
        self.action_indices = torch.arange(self.action_dims).to(device=self.device)
        self.action_vectors = torch.eye(self.action_dims).float().to(device=self.device)

    def forward(self, text_features):
        text_features = text_features.to(self.device)
        p = self.policy_head(text_features)
        policy = torch.distributions.Categorical(p)

        return policy

    def concatenate_actions(self, s):
        """Takes a tensor of embedding vectors and adds all actions for every vector"""
        s_rep = torch.repeat_interleave(s, self.action_dims, dim=0)
        a_rep = self.action_vectors.repeat(s.shape[0], 1)
        s_rep = s_rep.to(self.device)
        a_rep = a_rep.to(self.device)

        return (
            torch.cat((s_rep, a_rep), dim=1),
            s_rep.to(device=self.device),
            a_rep.to(device=self.device),
        )

    def get_action_probabilities(self, s):
        policy = self(s)
        action_probabilities = policy.probs
        z = action_probabilities == 0.0
        z = z.float() * 1e-8
        log_probs = torch.log(action_probabilities + z)

        return action_probabilities, log_probs

    def get_q_vals(self, s, target=True):
        sa, s_repeat, a_repeat = self.concatenate_actions(s)
        if target:
            q_vals1 = self.qtarget1(s_repeat, a_repeat).reshape(s.size(0), self.action_dims)
            q_vals2 = self.qtarget2(s_repeat, a_repeat).reshape(s.size(0), self.action_dims)
            q_vals = torch.min(q_vals1, q_vals2)
        else:
            q_vals1 = self.qsrc1(s_repeat, a_repeat).reshape(s.size(0), self.action_dims)
            q_vals2 = self.qsrc2(s_repeat, a_repeat).reshape(s.size(0), self.action_dims)
            q_vals = torch.min(q_vals1, q_vals2)

        return q_vals

    def train_critic(self, buffer):
        """Evaluates the state-action pairs of an episode.
        Returns the estimated value of each state and the
        log prob of the action taken
        """
        text, a, text_prime, r, terminal = buffer.sample()
        with torch.no_grad():
            s = text.to(self.device)
            self._s_rep = s.to(self.device)
            s_prime = text_prime.to(self.device)
            r = r.to(self.device)
            terminal = terminal.to(self.device)

            action_probs, log_probs = self.get_action_probabilities(s_prime)  # get all action probabilities and log probs
            q_prime = self.get_q_vals(s_prime, target=True)  # get all Q values
            q_prime = action_probs * (q_prime - self.alpha.exp().detach() * log_probs)  # compute expectation by weighing according to p
            q_prime = q_prime.sum(dim=1).unsqueeze(-1)  # integrate
            target = r + ((self.gamma * (1 - terminal)) * q_prime)

        s = s.to(self.device)
        a = a.to(self.device)
        q_vals1 = self.qsrc1(s, a)
        q_vals2 = self.qsrc2(s, a)

        crit = nn.MSELoss()
        loss1 = crit(q_vals1, target)
        loss2 = crit(q_vals2, target)

        return loss1 + loss2

    def train_actor(self):
        s = self._s_rep
        action_probs, log_probs = self.get_action_probabilities(
            s
        )  # get all action probabilities and log probs
        q_prime = self.get_q_vals(s, target=False)
        inside_term = self.alpha.exp().detach() * log_probs - q_prime
        loss = (action_probs * inside_term).sum(dim=1).mean()

        return loss

    def soft_update(self):
        """Updates the target network in the direction of the local network
        but by taking a step size less than one so the target network's parameter
        values trail the local networks. This helps stabilise training
        """
        for target_model, local_model in zip(self.target_models, self.src_models):
            for target_param, local_param in zip(target_model.parameters(), local_model.parameters()):
                target_param.data.copy_(
                    (self.rho * local_param.data)
                    + ((1.0 - self.rho) * target_param.data)
                )
