import random
import time
import numpy as np
import torch
import torch.optim as optim
import transformers
import argparse
from torch.utils.tensorboard import SummaryWriter
import time
from distutils.util import strtobool
from sac import LlamaSAC, SAC
from rb import Buffer
import gymnasium as gym
import gridworld


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--exp-name",
        type=str,
        default="Learning from coordinates",
        help="Experiment name",
    )
    parser.add_argument("--seed", type=int, default=1, help="Experiment seed")
    parser.add_argument(
        "--torch-deterministic",
        type=lambda x: bool(strtobool(x)),
        default=True,
        nargs="?",
        const=True,
        help="Use `torch.backends.cudnn.deterministic`",
    )
    parser.add_argument(
        "--cuda",
        type=lambda x: bool(strtobool(x)),
        default=True,
        nargs="?",
        const=True,
        help="Enable cuda",
    )
    parser.add_argument(
        "--track",
        type=lambda x: bool(strtobool(x)),
        default=False,
        nargs="?",
        const=True,
        help="Track with Weights and Biases",
    )
    parser.add_argument(
        "--wandb-project-name",
        type=str,
        default="Griworld",
        help="WandB project name",
    )
    parser.add_argument(
        "--wandb-entity",
        type=str,
        default="",
        help="WandB entity for this project",
    )

    parser.add_argument(
        "--hidden",
        type=int,
        default=512,
        help="Hidden dims of the policy MLP",
    )

    parser.add_argument(
        "--env-id", type=str, default="Gridworld", help="ID of the environment"
    )
    parser.add_argument(
        "--num-steps", type=int, default=15, help="Max episode length in the gridworld"
    )

    # SAC parameters
    parser.add_argument(
        "--num-episodes",
        type=int,
        default=1_000,
        help="N of episodes to complete",
    )
    parser.add_argument(
        "--buffer-size",
        type=int,
        default=int(100_000),
        help="Replay buffer size",
    )
    parser.add_argument("--gamma", type=float, default=0.99, help="Discount factor")

    parser.add_argument(
        "--rho", type=float, default=0.01, help="Target smoothing coefficient"
    )
    parser.add_argument(
        "--batch-size",
        type=int,
        default=24,
        help="Batch size of replay buffer sample",
    )
    parser.add_argument(
        "--learning-starts", type=int, default=250, help="When to start learning"
    )
    parser.add_argument(
        "--policy-lr",
        type=float,
        default=0.001,
        help="Learning rate of the policy network optimizer",
    )
    parser.add_argument(
        "--q-lr",
        type=float,
        default=0.001,
        help="Learning rate of the Q-network optimizer",
    )

    parser.add_argument(
        "--alpha", type=float, default=0.1, help="Entropy regularization coefficient"
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()

    run_name = f"gridworld_coordinates_{time.strftime('%Y-%m-%d_%H-%M-%S')}"
    if args.track:
        import wandb

        wandb.init(
            project=args.wandb_project_name,
            entity=args.wandb_entity,
            sync_tensorboard=True,
            config=vars(args),
            name=run_name,
            monitor_gym=False,
            save_code=True,
        )
    writer = SummaryWriter(f"runs/{run_name}")
    writer.add_text(
        "hyperparameters",
        "|param|value|\n|-|-|\n%s"
        % ("\n".join([f"|{key}|{value}|" for key, value in vars(args).items()])),
    )

    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.backends.cudnn.deterministic = args.torch_deterministic

    env = gym.make("Gridworld-v0", goal=(2, 3), starting_position=(1, 1))

    if torch.cuda.is_available():  # for nvidia GPUs
        device = torch.device("cuda:0")
    else:
        device = torch.device("cpu")

    num_episodes = args.num_episodes
    num_features = 2
    episode_length = args.num_steps
    agent = LlamaSAC(
        state_dims=2,
        hidden_dims=args.hidden,
        action_dims=env.action_space.n,
        gamma=args.gamma,
        alpha=args.alpha,
        rho=args.rho,
    ).to(device)
    optimizer_actor = optim.Adam(list(agent.policy_head.parameters()), args.policy_lr)
    optimizer_critics = optim.Adam(
        list(agent.qsrc1.parameters()) + list(agent.qsrc2.parameters()), args.q_lr
    )
    num_random = args.learning_starts
    states = torch.zeros(num_episodes, args.num_steps, 2)
    rewards = torch.zeros(num_episodes)

    buffer = Buffer(buffer_size=args.buffer_size)

    states = torch.zeros(num_episodes, episode_length, 2)
    rewards = torch.zeros(num_episodes)
    for episode in range(num_episodes):
        obs, info = env.reset()
        S = torch.zeros(episode_length, num_features)
        A = torch.zeros(episode_length, agent.action_dims)
        S_prime = torch.zeros_like(S)
        R = torch.zeros(episode_length, 1)
        terminal = torch.zeros(episode_length, 1)
        terminated, truncated, step = 0, 0, 0
        while not (terminated or truncated):
            S[step] = torch.from_numpy(obs['agent'])
            if episode < num_random:
                action = env.action_space.sample()
            else:
                action = agent.act(torch.from_numpy(obs['agent']).float()).item()
            next_obs, r, terminated, truncated, info = env.step(action)


            A[step] = torch.eye(agent.action_dims)[action]
            S_prime[step] = torch.from_numpy(next_obs['agent'])
            R[step] = r
            terminal[step] = torch.tensor([False]).float()

            obs = next_obs

            if episode >= 1:
                critic_loss = agent.train_critic(buffer)
                optimizer_critics.zero_grad()
                critic_loss.backward()
                optimizer_critics.step()
                if step % 2 == 0:
                    actor_loss = agent.train_actor()
                    optimizer_actor.zero_grad()
                    actor_loss.backward()
                    optimizer_actor.step()
                    agent.soft_update()

            step += 1
        buffer.append(S, A, S_prime, R, terminal)
        rewards[episode] = R.sum()
        if episode % 2 == 0:
            writer.add_scalar("charts/episodic_reward", R.sum().item(), episode)

        if episode > 2:
            writer.add_scalar("losses/critic_loss", critic_loss.item(), episode)
            writer.add_scalar("losses/actor_loss", actor_loss.item(), episode)

    writer.close()
